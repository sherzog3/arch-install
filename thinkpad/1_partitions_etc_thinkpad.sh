##################Make filesystems#####################
mkfs.vfat /dev/nvme0n1p1
mkfs.btrfs -R free-space-tree /dev/nvme0n1p2
mkfs.btrfs -R free-space-tree /dev/nvme1n1p1

#######################################################


##################Make BTRFS subvolumes################
mount /dev/nvme0n1p2 /mnt; cd /mnt

btrfs subvolume create @
btrfs subvolume create @home
btrfs subvolume create @var

cd; umount /mnt

mount /dev/nvme1n1p1 /mnt; cd /mnt

btrfs subvolume create @space

cd; umount /mnt

#######################################################


##################Setup mount points###################
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@ /dev/nvme0n1p2 /mnt
mkdir /mnt/{boot,home,var,space}/
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@home /dev/nvme0n1p2 /mnt/home
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@var /dev/nvme0n1p2 /mnt/var
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@space /dev/nvme1n1p1 /mnt/space
mount /dev/nvme0n1p1 /mnt/boot

#######################################################

#linux-zen
pacstrap /mnt base base-devel linux linux-firmware git vim amd-ucode btrfs-progs


genfstab -U /mnt >> /mnt/etc/fstab
mv /root/arch-install /mnt/root/
arch-chroot /mnt

#ls; cat /etc/fstab

#######################################################
printf "\e[1;32mDone! Run now the 2_base-uefi.sh script.\e[0m"


