
#make paru bottumup
sed -i 's/#BottomUp/BottomUp/g' /etc/paru.conf
sed -i 's/#SudoLoop/SudoLoop/g' /etc/paru.conf


#install thunderbird code and tmux
paru -S thunderbird code tmux nitrogen

#install and set up starhsip
sh -c "$(curl -fsSL https://starship.rs/install.sh)"

echo -e 'eval "$(starship init bash)"' >> ~/.bashrc

echo -e 'starship init fish | source' >> ~/.config/fish/config.fish


cp -r dotfiles/config/xmobar ~/.config/xmobar
cp -r dotfiles/config/alacritty ~/.config/alacritty
cp -r dotfiles/xmonad ~/.xmonad
cp dotfiles/config/starship.toml ~/.config/starship.toml


echo -e "set -g mouse on\nbind -n C-s set-window-option synchronize-panes" >> ~/.tmux.conf

#Install FantasqueSansMono
#wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FantasqueSansMono.zip

#ttf-nerd-fonts-symbols-mono
paru -S otf-fantasque-sans-mono ttf-fantasque-sans-mono nerd-fonts-fantasque-sans-mono ttf-nerd-fonts-symbols 