
#make paru bottumup
sudo sed -i 's/#BottomUp/BottomUp/g' /etc/paru.conf
sudo sed -i 's/#SudoLoop/SudoLoop/g' /etc/paru.conf

sudo seed -i 's/#Color/Color/g' /etc/pacman.conf

#install thunderbird code and tmux
paru -S thunderbird code tmux nitrogen

#install and set up starhsip
sh -c "$(curl -fsSL https://starship.rs/install.sh)"

echo -e 'eval "$(starship init bash)"' >> ~/.bashrc

echo -e 'starship init fish | source' >> ~/.config/fish/config.fish

echo -e "set -g mouse on\nbind -n C-s set-window-option synchronize-panes" >> ~/.tmux.conf


#ttf-nerd-fonts-symbols-mono
paru -S otf-fantasque-sans-mono ttf-fantasque-sans-mono nerd-fonts-fantasque-sans-mono ttf-nerd-fonts-symbols 