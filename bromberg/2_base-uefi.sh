sed -i 's/MODULES=()/MODULES=(btrfs)/g' /etc/mkinitcpio.conf
mkinitcpio -p linux

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=de-latin1-nodeadkeys" >> /etc/vconsole.conf
echo "bromberg" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 bromberg.localdomain bromberg" >> /etc/hosts
echo root:1234 | chpasswd

pacman -S efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools reflector base-devel linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils cups hplip alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion openssh rsync acpi acpi_call tlp virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld flatpak sof-firmware nss-mdns acpid os-prober ntfs-3g terminus-font

pacman -S nvidia nvidia-utils nvidia-settings

bootctl --path=/boot install 
uuid_boot=$(blkid -o value -s UUID /dev/sda2)

echo -e 'default arch.conf\ntimeout  3\n#console-mode max' > /boot/loader/loader.conf
echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux\ninitrd  /initramfs-linux.img\noptions  root=UUID='"$uuid_boot"' rootflags=subvol=@ rw' > /boot/loader/entries/arch.conf
echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux\ninitrd  /initramfs-linux-fallback.img\noptions  root=UUID='"$uuid_boot"' rootflags=subvol=@ rw' > /boot/loader/entries/arch-fallback.conf

bootctl --path=/boot update

systemctl enable NetworkManager
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

useradd -m sherzog
echo sherzog:1234 | chpasswd
usermod -aG libvirt sherzog

echo "sherzog ALL=(ALL) ALL" >> /etc/sudoers.d/sherzog

printf "\e[1;32mDone! Type exit, umount -R /mnt and reboot.\e[0m"
