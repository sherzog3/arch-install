#!/bin/bash

sed -i 's/MODULES=()/MODULES=(btrfs)/g' /etc/mkinitcpio.conf
mkinitcpio -p linux

ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=de-latin1-nodeadkeys" >> /etc/vconsole.conf
echo "bromberg" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 bromberg.localdomain bromberg" >> /etc/hosts
echo root:1234 | chpasswd

# You can add xorg to the installation packages, I usually add it at the DE or WM install script
# You can remove the tlp package if you are installing on a desktop or vm

pacman -S efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools reflector base-devel linux-headers avahi xdg-user-dirs xdg-utils gvfs gvfs-smb nfs-utils inetutils dnsutils bluez bluez-utils cups hplip alsa-utils pipewire pipewire-alsa pipewire-pulse pipewire-jack bash-completion openssh rsync acpi acpi_call tlp virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld flatpak sof-firmware nss-mdns acpid os-prober ntfs-3g terminus-font

#pacman -S nvidia nvidia-utils nvidia-settings
#or xf86-video-amdgpu

# pacman -S grub grub-btrfs
# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB #change the directory to /boot/efi is you mounted the EFI partition at /boot/efi
# grub-mkconfig -o /boot/grub/grub.cfg

bootctl --path=/boot install 
uuid_boot=$(blkid -o value -s UUID /dev/vda2)

echo -e 'default arch.conf\ntimeout  3\n#console-mode max' > /boot/loader/loader.conf
#echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux\ninitrd  /amd-ucode.img\ninitrd  /initramfs-linux.img\noptions  root="LABEL=boot" rw' > /boot/loader/entries/arch.conf
#echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux\ninitrd  /amd-ucode.img\ninitrd  /initramfs-linux-fallback.img\noptions  root="LABEL=boot" rw' > /boot/loader/entries/arch-fallback.conf
echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux\ninitrd  /initramfs-linux.img\noptions  root=UUID='"$uuid_boot"' rootflags=subvol=@ rw' > /boot/loader/entries/arch.conf
echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux\ninitrd  /initramfs-linux-fallback.img\noptions  root=UUID='"$uuid_boot"' rootflags=subvol=@ rw' > /boot/loader/entries/arch-fallback.conf

bootctl --path=/boot update

systemctl enable NetworkManager
systemctl enable cups.service
systemctl enable sshd
systemctl enable avahi-daemon
systemctl enable reflector.timer
systemctl enable fstrim.timer
systemctl enable libvirtd
systemctl enable firewalld
systemctl enable acpid

useradd -m sherzog
echo sherzog:1234 | chpasswd
usermod -aG libvirt sherzog

echo "sherzog ALL=(ALL) ALL" >> /etc/sudoers.d/sherzog



printf "\e[1;32mDone! Type exit, umount -R /mnt && reboot.\e[0m\n"



#alternativ zu grub
#########################
# bootctl --path=/boot install 
#
# echo -e 'default arch.conf\ntimeout  4\n#console-mode max' > /boot/loader/loader.conf
# echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux\ninitrd  /amd-ucode.img\ninitrd  /initramfs-linux.img' > /boot/loader/entries/arch.conf
# cp /boot/loader/entries/arch.conf /boot/loader/entries/arch-fallback.conf
# sed -i 's/initrd  /initramfs-linux.img/initrd/initramfs-linux-fallback.img/g' /boot/loader/entries/arch-fallback.conf
#########################


# bootctl --path=/boot install 
# echo -e 'default arch.conf\ntimeout  4\n#console-mode max' > /boot/loader/loader.conf
# echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux\ninitrd  /amd-ucode.img\ninitrd  /initramfs-linux.img' > /boot/loader/entries/arch.conf
# echo -e 'title   Arch Linux\nlinux   /vmlinuz-linux\ninitrd  /amd-ucode.img\ninitrd  /initramfs-linux-fallback.img' > /boot/loader/entries/arch-fallback.conf
