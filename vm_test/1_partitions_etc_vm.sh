##################Partitionen anlegen##################
sgdisk --zap-all /dev/vda
sgdisk -n 0:0:+500M -t 0:ef00 -c 0:"boot" /dev/vda
sgdisk -n 0:0:+19G -t 0:8300 -c 0:"root" /dev/vda

partprobe /dev/vda
##################Make filesystems#####################
mkfs.fat -F32 /dev/vda1
mkfs.btrfs -R free-space-tree /dev/vda2

#######################################################


##################Make BTRFS subvolumes################
mount /dev/vda2 /mnt; cd /mnt

btrfs subvolume create @
btrfs subvolume create @var
btrfs subvolume create @home

cd; umount /mnt

#######################################################


##################Setup mount points###################
mount -o noatime,compress=zstd,space_cache=v2,ssd,discard=async,subvol=@ /dev/vda2 /mnt
mkdir /mnt/{boot,home,var}/
mount -o noatime,compress=zstd,space_cache=v2,ssd,discard=async,subvol=@home /dev/vda2 /mnt/home
mount -o noatime,compress=zstd,space_cache=v2,ssd,discard=async,subvol=@var /dev/vda2 /mnt/var

mount /dev/vda1 /mnt/boot

#######################################################

#linux-zen und oder amd-ucode/intel-ucode 
pacstrap /mnt base base-devel linux linux-firmware git vim amd-ucode btrfs-progs


genfstab -U /mnt >> /mnt/etc/fstab
mv /root/arch-install /mnt/root/
printf "\e[1;32mDone! Run now the 2_base-uefi.sh script.\e[0m"
arch-chroot /mnt

#ls; cat /etc/fstab
#######################################################


