#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c Germany -a 15 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Sy

##################install paru#########################
cd /tmp
git clone https://aur.archlinux.org/paru.git
cd paru/ 
makepkg -si

##################install paru#########################
sudo localectl --no-convert set-keymap de-latin1-nodeadkeys
sudo localectl --no-convert set-x11-keymap de pc105 deadgraveacute

sudo sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 5/g' /etc/pacman.conf


##################create zram########################## 
paru -S zramd 
#hier kann man was ändern muss man aber nicht
#sudo vim /etc/default/zramd 
sudo systemctl enable --now zramd.service 
#######################################################



#https://wiki.archlinux.org/title/xrandr

sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload

sudo pacman -S sddm adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts adobe-source-sans-fonts alacritty arc-gtk-theme arc-icon-theme bash bat cups dina-font element-desktop emacs exa fd firefox fish gentium-plus-font gnu-free-fonts htop inter-font lolcat lxappearance lxsession maim mpv neovim noto-fonts noto-fonts-cjk noto-fonts-emoji obs-studio opendoas otf-font-awesome pacman-contrib pandoc pcmanfm picom qalculate-gtk qt5ct qutebrowser ripgrep sddm shellcheck simplescreenrecorder stack starship sxiv tamsyn-font tex-gyre-fonts trayer ttf-anonymous-pro ttf-bitstream-vera ttf-cascadia-code ttf-croscore ttf-dejavu ttf-droid ttf-fantasque-sans-mono ttf-fira-code ttf-fira-mono ttf-ibm-plex ttf-inconsolata ttf-jetbrains-mono ttf-joypixels ttf-junicode ttf-liberation ttf-linux-libertine ttf-monofur ttf-opensans ttf-roboto vlc volumeicon xdg-user-dirs xdotool xf86-video-qxl xmobar xmonad xmonad-contrib xorg xorg-server xorg-xkill xorg-xmessage xorg-xrdb xorg-xsetroot xterm xwallpaper zathura zsh plasma kde-applications papirus-icon-theme kdenlive materia-kde

sudo systemctl enable sddm
sudo virsh net-autostart default

paru -S timeshift timeshift-autosnap 

/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"

