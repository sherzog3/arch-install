##################Partitionen anlegen##################
wipefs -a /dev/sda*
wipefs -a /dev/sdb*

sgdisk -Z /dev/sda

sgdisk -n 0:0:+500M -t 0:ef00 /dev/sda
sgdisk -n 0:0:+230G -t 0:8300 /dev/sda

sgdisk -n 0:0:+1700G -t 0:8300 /dev/sdb

partprobe /dev/sda
partprobe /dev/sdb

##################Make filesystems#####################
mkfs.vfat /dev/sda1
mkfs.btrfs -R free-space-tree /dev/sda2
mkfs.btrfs -R free-space-tree /dev/sdb1

#######################################################


##################Make BTRFS subvolumes################
mount /dev/sda2 /mnt; cd /mnt

btrfs subvolume create @
btrfs subvolume create @var

cd; umount /mnt

mount /dev/sdb1 /mnt; cd /mnt

btrfs subvolume create @home

cd; umount /mnt

#######################################################


##################Setup mount points###################
mount -o noatime,compress=zstd,space_cache=v2,ssd,discard=async,subvol=@ /dev/sda2 /mnt

mkdir /mnt/{boot,home,var,space}/
mount -o noatime,compress=zstd,space_cache=v2,subvol=@home /dev/sdb1 /mnt/home
mount -o noatime,compress=zstd,space_cache=v2,discard=async,subvol=@var /dev/sda2 /mnt/var
mount /dev/sda1 /mnt/boot

#######################################################

#linux-zen und oder amd-ucode/intel-ucode 
pacstrap /mnt base base-devel linux linux-firmware git vim amd-ucode btrfs-progs


genfstab -U /mnt >> /mnt/etc/fstab
mv /root/arch-install /mnt/root/
printf "\e[1;32mDone! Run now the 2_base-uefi.sh script.\e[0m"
arch-chroot /mnt

#ls; cat /etc/fstab
#######################################################


