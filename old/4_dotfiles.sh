#!/bin/bash

sh -c "$(curl -fsSL https://starship.rs/install.sh)"

cp -r dotfiles/config/alacritty ~/.config/alacritty
cp -r dotfiles/config/conky ~/.config/conky
cp -r dotfiles/config/fish ~/.config/fish
cp -r dotfiles/config/xmobar ~/.config/xmobar


